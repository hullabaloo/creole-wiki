<?php


// Create wiki file

$page = str_replace(' ','_',ltrim(urldecode($_GET['page']),'/'));

if (strpos($page, '..')!==false) die('Access denied');

$file = $_SERVER['DOCUMENT_ROOT'].$page.'.wiki';

if (!file_exists($file)) {
	$title = str_replace('_', ' ', $page);

	$lastSlash = strrpos($page,'/');
	if ($lastSlash!==false)
		$title = substr($title,$lastSlash+1);

	if (!is_dir(dirname($file)))
		mkdir(dirname($file),0777,true);
	file_put_contents($file, sprintf("= %s =\n\n",$title));
}

if ($_SERVER['REMOTE_ADDR']==$_SERVER['SERVER_ADDR']) {
	shell_exec("/usr/local/bin/subl ".escapeshellarg($file));
}

header('Location: /'.$page);

?>