<?php

/**
 * Render a Creole format page file
 *
 * Using https://github.com/codeholic/w
 * @author  Matt Dolan <matt@hullabaloo.co.uk>
 */

// require_once('Text/Wiki.php');
// require_once('Text/Wiki/Creole.php');
// $parser = new Text_Wiki_Creole();

require_once('lib/creole.php');




$page = str_replace(' ','_',ltrim(urldecode($_GET['page']),'/'));

if (strpos($page, '..')!==false) die('Access denied');

$title = str_replace('_', ' ', $page);
$lastSlash = strrpos($page,'/');
$shortTitle = ($lastSlash!==false) ? substr($title,$lastSlash+1) : $title;


$creole = new creole(
    array(
        // convert spaces to underscores
       'link_format' => create_function('$s',"return '/'.str_replace(' ', '_', \$s);"),
       'encode_links' => false,
        'interwiki' => array(
            'WikiCreole' => 'http://www.wikicreole.org/wiki/%s',
            'Wikipedia' => 'http://en.wikipedia.org/wiki/%s'
        )
    )
);


$file = $_SERVER['DOCUMENT_ROOT'].$page.'.wiki';

$toolbar = '';

$fileExists = file_exists($file);

// Allow the files to have spaces rather than underscores
if (!$fileExists && file_exists(preg_replace('%(?<!/)_%', ' ', $file))) {
    $file = preg_replace('%(?<!/)_%', ' ', $file);
    $fileExists = true;
}

if ($fileExists) {
    $content = file_get_contents($file);
    // workaround bug
    $content = str_replace('{{{0}}}','{{{ 0}}}',$content);

    $output = $creole->parse($content);


    $toolbar .= '<span class="dateModified">Modified '.date('jS M Y H:i:s',filemtime($file)).'</span>';
    // On same machine, show Edit button
    if ($_SERVER['REMOTE_ADDR']==$_SERVER['SERVER_ADDR']) {
        $toolbar .= '<a href="'.$_SERVER['REQUEST_URI'].'?edit">Edit in Sublime</a>
                        <a href="#" id="createChild">Create Subpage</a>';
    }
} else {
    $output = '<h1>'.htmlspecialchars($shortTitle).'</h1><p>Page not found.';
    if ($_SERVER['REMOTE_ADDR']==$_SERVER['SERVER_ADDR']) {
        $output .= ' <a href="/'.$page.'?edit">Create it</a></p>';
    }
}





function listFiles($dir) {
    $out = array();
    $dir = rtrim($dir.'/').'/';
    $files = glob($dir.'*');
    foreach ($files as $f) {
        $basename = basename($f,'.wiki');
        if ($basename[0]=='_') continue;

        if (is_dir($f)) {
            $out[$basename] = listFiles($f);
        } else if (!$out[$basename] && strpos($f, '.wiki')!==false) {
            $out[$basename] = 'file';
        }
    }
    return $out;
}


function printFiles($files,$path = '/') {
    print '<ul>';
    foreach ($files as $file=>$dir) {
        print '<li';
        if ($_SERVER['REQUEST_URI']==$path.$file)
            print ' class="active"';
        print '><a href="'.$path.$file.'">'.htmlspecialchars(str_replace('_',' ',$file)).'</a>';
        if (is_array($dir))
            printFiles($dir,$path.$file.'/');
        print '</li>';
    }
    print '</ul>';
}


?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title><?=join(' :: ',array_reverse(explode('/', $title)))?></title>
    <link rel="stylesheet" href="/bitbucket.css"/>
    <link rel="stylesheet" href="/override.css"/>
    <script src="//ajax.googleapis.com/ajax/libs/prototype/1.6.1/prototype.js" type="text/javascript" charset="utf-8" defer></script>
    <script src="/wiki.js" type="text/javascript" charset="utf-8" defer></script>

</head>
<body>
    <div id="container">
        <nav>
            <?

            $files = listFiles($_SERVER['DOCUMENT_ROOT']);
            printFiles($files);

            ?>
        </nav>

        <div class="sane-defaults sane-defaults-2" id="wiki">
            <?=$output?>
            <? if ($toolbar): ?>
                <div id="toolbar"><?=$toolbar?></div>
            <? endif; ?>
        </div>
    </div>
</body>
</html>

